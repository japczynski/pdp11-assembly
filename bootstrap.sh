#!/bin/sh

BINUTILS_URL=https://ftp.gnu.org/gnu/binutils
BINUTILS=binutils-2.33.1
EXT=.tar.gz

GCC_URL=https://ftp.gnu.org/gnu/gcc/gcc-9.2.0
GCC=gcc-9.2.0
EXT=.tar.gz


echo "This script will bootstrap a pdp-11 build chain.  It will download
binutils and gcc, configure both for pdp-11/a.out format, and install them
to a local directory at ./bootstrap/pdp11

It will also clone and build a utility called bin2load that converts a.out
files to pdp-11 absolute loader paper tape format.  This format is suitable for
loading in simh with the 'load' command."

read -p "Build binutils? [y/n]: " build_binutils
read -p "Build gcc? [y/n]: " build_gcc
read -p "Build bin2load? [y/n]: " build_bin2load

make_dir () {
    mkdir -p bootstrap
    cd bootstrap
    BOOTSTRAP_DIR=$(pwd)
}

download () {
    cd ${BOOTSTRAP_DIR}
    if [ $build_binutils = 'y' ]; then
        wget ${BINUTILS_URL}/${BINUTILS}${EXT}
    fi
    if [ $build_gcc = 'y' ]; then
        wget ${GCC_URL}/${GCC}${EXT}
        sudo apt-get install libgmp-dev libmpfr-dev libmpc-dev
    fi
    if [ $build_bin2load = 'y' ]; then
        git clone https://github.com/jguillaumes/retroutils.git
    fi
}

install_binutils () {
    cd ${BOOTSTRAP_DIR}
    tar -xzvf ${BINUTILS}${EXT}
    cd ${BINUTILS}
    mkdir build
    cd build
    ../configure --target=pdp11-aout --prefix=${BOOTSTRAP_DIR}/pdp11
    make
    make install
}

install_gcc () {
    cd ${BOOTSTRAP_DIR}
    tar -xzvf ${GCC}${EXT}
    cd ${GCC}
    mkdir build
    cd build
    ../configure --target=pdp11-aout \
                 --prefix=${BOOTSTRAP_DIR}/pdp11 \
                 --enable-languages=c,c++ \
                 --disable-libstdc++-v3 \
                 --disable-libssp \
                 --disable-libgfortran \
                 --disable-libobjc
    make
    make install
}

install_bin2load () {
    cd ${BOOTSTRAP_DIR}/retroutils/bin2load
    make
    mkdir -p ${BOOTSTRAP_DIR}/pdp11/bin
    cp bin2load ${BOOTSTRAP_DIR}/pdp11/bin/
}


if [ $build_binutils = "y" -o $build_gcc = "y"  -o $build_bin2load = "y" ];
then
    make_dir
    download
    if [ $build_binutils = "y" ]; then
        install_binutils
    fi
    if [ $build_gcc = "y" ]; then
        install_gcc
    fi
    if [ $build_bin2load = "y" ]; then
        install_bin2load
    fi
fi
