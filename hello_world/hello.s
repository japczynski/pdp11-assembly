	.global hello
	serial = 0177560	# base address of DL11
				# buffer offsets are
				# 0: rcsr - receiver status register
				# 2: rbuf - receiver data buffer register
				# 4: xcsr - transmitter status register
				# 6: xbuf - transmitted data buffer register

	.text
hello:
	mov	2(sp), r1	# get string pointer off the stack
	mov	$serial+4,r2	# xcsr is 4 bytes after DL11 base address
nxtchr:
	movb	(r1)+, r0	# get the next character and increment the string pointer
	beq	done		# NULL == end of string
	movb	r0, 2(r2)	# fill xbuf with a character, xbuf is 2 bytes after xcsr
wait:
	tstb	(r2)		# test the low byte pointed to by r2
				# bit 7 of r2 is the transmitter ready bit
	bpl	wait		# if that bit is 0, the byte is positive and we wait
	br	nxtchr		# if that bit is 1, the byte is negative and we go to the next char
done:
	rts	pc
