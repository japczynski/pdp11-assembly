	.global start
	.global return

	.text

start:
	mov	$0776, sp		# initialize the stack

main:
	mov	$hello_str, -(sp)	# push string pointer on stack
	jsr	pc, hello
	add	$2, sp			# pop string pointer off stack
	halt

	.data
	.align 2
hello_str:
	.ascii	"Hello, World!"
	.byte	12, 0
