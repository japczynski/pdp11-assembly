	.global start

	dl11 = 0177560	# base address of dl11
			# buffer offsets are
			# 0: rcsr - receiver status register
			# 2: rbuf - receiver data buffer register
			# 4: xcsr - transmitter status register
			# 6: xbuf - transmitted data buffer register

	.text
start:
	mov	$start, sp		# initialize the stack
	clr	r0
clear_vectors:
	clr	(r0)+
	cmp	r0, $start
	bne	clear_vectors
	mov	$dl11_int, 060		# set dl11 interrupt vector
	mov	$0, 062
	jsr	pc, main		# jump to main
	halt
	halt


dl11_int:
	rti				# for now, immediately return


main:
	# # cons_puts(hello_str, dl11)
	# mov	$dl11, -(sp)
	# mov	$hello_str, -(sp)
	# jsr	pc, cons_puts
	# add	$04, sp

	mov	$dl11, -(sp)
	sub	$02, sp
mainloop:
	mov	$cmdline, (sp)
	jsr	pc, cons_getline

	mov	$cmdline, -(sp)
	mov	$quitstr, -(sp)
	jsr	pc, strcmp
	add	$04, sp

	jsr	pc, cons_puts
	mov	$015, (sp)
	jsr	pc, cons_putc
	mov	$012, (sp)
	jsr	pc, cons_putc

	tst	r0
	beq mainloop

	add	$04, sp

	rts	pc

	# int* addr = 0
	# char cmdline[256]
	# while(true):
	# 	cmdline = gets(dl11)
	#	if cmdline == 'r':
	#		saveVectorTable()
	# 		saveRegisters()
	# 		jsr pc, addr
	# 	elif cmdline[-1] == ':'
	# 		addr = atoi(cmdline[:-1])
	# 	else:
	# 		*addr++ = atoi(cmdline)


# bool strcmp(const char* str1, const char* str2)
strcmp:
	mov	r1, -(sp)
	mov	r2, -(sp)
	mov	06(sp), r1
	mov	010(sp), r2
	mov	$0, r0

strcmploop:
	cmpb	(r1), (r2)
	bne	strcmp_ret
	add	$01, r2
	tstb	(r1)+
	bne	strcmploop
	mov	$01, r0

strcmp_ret:
	mov	(sp)+, r2
	mov	(sp)+, r1
	rts	pc


	.data
	.align 2
hello_str:
	.ascii "Hello, World!\r\n\0"

	.align 2
quitstr:
	.ascii "quit\0"

	.align 2
cmdline:
	.ascii "12345\0"
	.space 256
