	.global cons_puts
	.global cons_putc
	.global cons_getc
	.global cons_getline

	.text
# void cons_putc(const char c, const uint16_t* device)
cons_putc:
	mov	r0, -(sp)
	# stack:
	# -6 pointer-to-device
	# -4 character value
	# -2 return address
	# -0 old r0

	mov	06(sp), r0	# pointer to device
cons_putc_wait:
	tstb	04(r0)		# xcsr is 4 bytes past device base addr
	bpl	cons_putc_wait	# wait for device to be ready
	movb	04(sp), 06(r0)	# xbuf is 06 bytes past device base addr
				# char is 04 bytes down the stack
cons_putc_wait2:
	tstb	04(r0)
	bpl	cons_putc_wait2

	mov	(sp)+, r0	# restore registers
	rts	pc		# return


# void cons_puts(const char* str, const uint16_t* device)
cons_puts:
	mov	r0, -(sp)
	mov	04(sp), r0	# r0 = str

	# while(*r0) { cons_putc(*ro++, device) };
	mov	06(sp), -(sp)	# push device
	movb	(r0)+, -(sp)	# push *r0++
loop:
	jsr	pc, cons_putc
	movb	(r0)+, (sp)	# replace arg on stack instead of pop/push
	bne	loop
	add	$04, sp		# clean up

	mov	(sp)+, r0
	rts	pc


# char cons_getc(const uint16_t* device)
cons_getc:
	mov	02(sp), r0	# r1 = device
cons_getc_wait:
	tstb	(r0)		# wait for uart done signal
	bpl	cons_getc_wait
	movb	02(r0), r0	# copy byte to r0 for return
	rts	pc


# void cons_getline(char *strbuf, uint16_t* device)
cons_getline:
	mov	r0, -(sp)
	mov	r1, -(sp)

	mov	06(sp), r1	# r1 = strbuf
	mov	010(sp), -(sp)	# push device
cgl_getline:
	# *strbuf = cons_getc(r1, device)
	jsr	pc, cons_getc
	movb	r0, (r1)

	// echo
	mov	r0, -(sp)
	jsr	pc, cons_putc
	add	$02, sp

	# loop until carraige return and increment pointer
	cmpb	(r1)+, $015
	bne	cgl_getline

	// echo newline after carraige return
	mov	$012, -(sp)
	jsr	pc, cons_putc
	add	$04, sp

	// terminate string with '\0'
	// replace carraige return with '\0'
	movb	$0, -(r1)

	mov	(sp)+, r1
	mov	(sp)+, r0
	rts	pc
